import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Signin from './user/Signin';
import Signup from './user/Signup';
import Home from './core/Home';
import PrivateRoute from './auth/PrivateRoute';
import AdminRoute from './auth/AdminRoute';
import UserDashboard from './user/UserDashboard';
import AdminDashboard from './user/AdminDashboard';
import CreateCategory from './admin/AddCategory'
import CreateProduct from './admin/AddProduct';
import Shop from './core/Shop';
import Product from './core/Product';

const Routes = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route path='/' exact component={Home} />
                <Route path='/signin' exact component={Signin} />
                <Route path='/signup' exact component={Signup} />
                <Route path='/shop' exact component={Shop} />
                <PrivateRoute 
                    path='/user/dashboard'
                    exact 
                    component={UserDashboard}/>
                <AdminRoute 
                    path='/admin/dashboard'
                    exact
                    component={AdminDashboard}/>
                <AdminRoute 
                    path='/create/category'
                    exact
                    component={CreateCategory}/>
                <AdminRoute 
                    path='/create/product'
                    exact
                    component={CreateProduct}/>
                <Route path='/product/:productId' exact component={Product} />
                
            </Switch>
        </BrowserRouter>
    );
}

export default Routes;