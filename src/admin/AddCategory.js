import React, {useState} from 'react'
import Layout from '../core/Layout';
import { isAuthenticated } from '../auth'
import { Link } from 'react-router-dom';
import { createCategory } from './apiAdmin';

const AddCategory = () => {
    const [name, setName] = useState('');
    const [error, setError] = useState(false);
    const [success, setSuccess] = useState(false)

    // destructur user and token from localstorage
    const {user, token} = isAuthenticated();

    const handleChange = (e) => {
        setError('')
        setName(e.target.value)
    }

    const clickSubmit = (e) => {
        e.preventDefault()
        setError('')
        setSuccess(false)
        // make request to api to create category
        createCategory(user._id, token, {name})
            .then(data => {
                if (data.error) {
                    setError(data.error)
                } else {
                    setError(false)
                    setSuccess(true)
                }
            });

    }

    const showError = () => 
        error && (
            <div>
                <h3 className="alert alert-danger">
                    {error}
                </h3>
            </div>
        )

    const showSuccess = () =>
        success && (
            <div>
                <h3 className="alert alert-success">
                    {name} is created
                </h3>
            </div>
        )
    
    const getBack = () => 
        <div className="mt-5">
            <Link to="/admin/dashboard" className="text-warning">
                Back to dashboard
            </Link>
        </div>

    const addCategoryForm = () => (
        <form onSubmit={clickSubmit}>
            <div className="form-group">
                <label className="text-muted">Name</label>
                <input 
                    type="text" 
                    className="form-control" 
                    onChange={handleChange} 
                    value={name}
                    autoFocus 
                />
            </div>
            <button className="btn btn-outline-primary">
                Create Category
            </button>
        </form>
    )

    return (
        <Layout 
            title = "Add a new category"
            description="Add category to Ecommerce app"
            className="container col-md-8 offset-md-2"
        >
            <div className="row">
                <div className="col-md-8 offset-md-2">
                    {showError()}
                    {showSuccess()}
                    {addCategoryForm()}
                    {getBack()}
                </div>
            </div>
        </Layout>
    )
}

export default AddCategory;