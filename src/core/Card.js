import React from 'react';
import {Link} from 'react-router-dom';
import ShowImage from './showImage';
import moment from 'moment';

const Card = ({ product, showViewProductButton=true }) => {

	const showViewButton = showViewProductButton => (
		showViewProductButton && (
			<Link to={`/product/${product._id}`}>
				<button className="btn btn-outline-primary mt-2 mb-2 mr-2">
					View product
				</button>
			</Link>
		)
	)

	const showAddToCartButton = () => (
		<button className="btn btn-outline-success mt-2 mb-2">
			Add to card
		</button>
	)

	const showInStock = (quantity) => {
		return quantity > 0 ? (
			<span className="badge badge-primary badge-pill">In Stock</span>
		) : <span className="badge badge-primary badge-pill">Out of Stock</span>
	}

  	return (
		<div className="card">
			<div className="card-header">{product.name}</div>
			<div className="card-body">
				<ShowImage item={product} url="product" />
				<p className="lead mt-2">{product.description.substring(0, 100)}</p>
				<p className="black-9">${product.price}</p>
				<p className="black-8">
					Category: {product.category && product.category.name}
				</p>
				<p class="black-8">
					Added on {moment(product.createdAt).fromNow()}
				</p>
				{ showViewButton(showViewProductButton) }
				{ showInStock(product.quantity) }
				<br/>

				{ showAddToCartButton() }
				
			</div>
		</div>
	)
}

export default Card;