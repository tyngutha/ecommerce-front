import React, {useState} from 'react';

const Checkbox = ({categories, handleFilters}) => {
	const [checked, setChecked] = useState([]);

	const handleToggle = c => () => {
		// return first index or -1
		const currentCategoryId = checked.indexOf(c);
		const newCheckedCategoryId = [...checked]
		// if currently checked was not already in checked state > push
		// else pull/take off
		if (currentCategoryId === -1) {
			newCheckedCategoryId.push(c);
		} else {
			newCheckedCategoryId.splice(currentCategoryId, 1);
		}
		setChecked(newCheckedCategoryId)
		// use a callback function to pass data from a child to it's parent component Shop
		handleFilters(newCheckedCategoryId);
	}

	return categories.map((c, i) => (
			<li key={i} className="list-unstyled">
					<input 
						type="checkbox" 
						onChange={handleToggle(c._id)}
						className="form-check-input"
					/>
					<label className="form-check-label">{c.name}</label>
			</li>
	))
}

export default Checkbox;