import React, { useState, useEffect } from 'react';
import Layout from './Layout';
import {getProducts} from './apiCore';
import Card from './Card';
import Search from './Search';

const Home = () => {
	const [productBySell, setProductBySell] = useState([]);
	const [productByArrival, setProductByArrival] = useState([]);
	const [error, setError] = useState(false);

	const loadProductBySell = () => {
		getProducts('sold').then(data => {
			if (data.error) {
				setError(data.error)
			} else {
				setProductBySell(data)
			}
		})
	}

	const loadProductByArrival = () => {
		getProducts('createdAt').then(data => {
			if (data.error) {
				setError(data.error)
			} else {
				setProductByArrival(data)
			}
		})
	}

	useEffect(() => {
		loadProductBySell()
		loadProductByArrival()
	}, [])

	return (
		<Layout 
				title = "Home Page"
				description="Node React E-commerce App" 
				className="container-fluid"
		>
			<Search />
			<h2 className="mb-4">New Arrival</h2>
			<div className="row">
				{
					productByArrival.map((p, i) => (
						<div className="col-3 mb-3" key={i}>
							<Card product={p} />
						</div>
					))
				}
			</div>

			<h2 className="mb-4">Best sellers</h2>
			<div className="row">
				{
					productBySell.map((p, i) => (
						<div className="col-3 mb-3" key={i}>
							<Card product={p} />
						</div>
					))
				}
			</div>
		</Layout>
	)
}
export default Home;