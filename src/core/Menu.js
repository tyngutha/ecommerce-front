import React, {Fragment} from 'react';
import {Link, withRouter} from 'react-router-dom';
import { signout, isAuthenticated } from "../auth";

const isActive = (history, path) => {
    if (history.location.pathname === path) {
        return {color: "#ff9900"};
    } else {
        return { color: "#ffffff"};
    }
};


const Menu = ({ history }) => {

    const isAuth = isAuthenticated();

    
    const linkToDashboard = () => {
        if (isAuth) {
            const {user: {_id, name, email, role}} = isAuth
            if (role === 0) {
                return "/user/dashboard";
            } else {
                return "/admin/dashboard";
            }
        } else {
            return "/user/dashboard";
        }
    }

    return (
        <div>
            <ul className="nav nav-tabs bg-primary">
                <li className="nav-item">
                    <Link
                        className="nav-link"
                        style={isActive(history, "/")}
                        to="/">
                        Home
                    </Link>
                </li>

                <li className="nav-item">
                    <Link
                        className="nav-link"
                        style={isActive(history, "/shop")}
                        to="/shop">
                        Shop
                    </Link>
                </li>

                <li className="nav-item">
                    <Link
                        className="nav-link"
                        style={isActive(history, "/user/dashboard")}
                        to={linkToDashboard()}
                        >
                        Dashboard
                    </Link>
                </li>   

                { !isAuthenticated() && (
                    <Fragment>
                        <li className="nav-item">
                            <Link
                                className="nav-link" 
                                style={isActive(history, "/signin")}
                                to="/signin">
                                Signin
                            </Link>
                        </li>

                        <li className="nav-item">
                            <Link 
                                className="nav-link"
                                style={isActive(history, "/signup")}
                                to="/signup">
                                Signup
                            </Link>
                        </li>
                    </Fragment>
                )}

                { isAuthenticated() && (
                    <li className="nav-item">
                        <span 
                            className="nav-link"
                            style={{ cursor: "pointer", "color": "#ffffff" }}
                            onClick={() => signout(() => {
                                history.push("/");
                            })}>
                            Signout
                        </span>
                    </li>
                )}
            </ul>
        </div>
    )
};

export default withRouter(Menu);