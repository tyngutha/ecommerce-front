import React, { useState, useEffect } from 'react';
import Layout from './Layout';
import { read, listRelated } from './apiCore';
import Card from './Card';

const Product = (props) => {
    const [product, setProduct] = useState({})
    const [error, setError] = useState(false)
    const [relatedProduct, setRelatedProduct] = useState([])

    const loadSingleProduct = productId => {
        read(productId).then(data => {
            if (data.error) {
                setError(data.error)
            } else {
                setProduct(data);
                listRelated(data._id).then(data => {
                    if (data.error) {
                        setError(data.error)
                    } else {
                        setRelatedProduct(data)
                    }
                })
            }
        })
    }

    useEffect(() => {
        // grab product id from url
        const productId = props.match.params.productId
        loadSingleProduct(productId)
    }, [])

    return (
        <Layout 
            title={product && product.name}
            description={ product && product.description && product.description.substring(0, 100)} 
            className="container-fluid"
		>
            <div className="row">
                <div className="col-lg-4">
                    {product && product.description && <Card product={product} showViewProductButton={false} />}
                </div>
                <div className="col-lg-8">
                    <p>Order product...</p>
                </div>
            </div>
            <h2>Related Products</h2>
            <div className="row">
                {
                    relatedProduct.map((p, i) => (
                        <div className="col-lg-3" key={i}>
                            <Card product={p}/>
                        </div>
                    ))
                }
        

            </div>
		</Layout>
    )
}

export default Product;