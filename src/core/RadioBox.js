import React, {useState} from 'react';
import { prices } from './fixedPrices';

const RadioBox = ({prices, handleFilters}) => {
	const [value, setValue] = useState([]);

	const handleChange = (event) => {
		let currentValue = [];
		const priceId = parseInt(event.target.value);
		prices.map((price) => {
			if (price._id === priceId) {
				currentValue = price.array
			}
		});
		setValue(currentValue);
		handleFilters(currentValue);
	}

	return prices.map((p, i) => (
		<div key={i}>
			<input
				value={`${p._id}`}
				onChange={handleChange} 
				type="radio" 
				name={p}
				className="mr-2 ml-2"
			/>
			<label>{p.name}</label>
		</div>
	))
}

export default RadioBox;