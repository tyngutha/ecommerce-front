import React, { useState, useEffect } from 'react';
import Layout from './Layout';
import Card from './Card';
import { getCategories, getFilteredProducts } from './apiCore';
import Checkbox from './Checkbox';
import RadioBox from './RadioBox';
import { prices } from './fixedPrices';

const Shop = () => {
	const [categories, setCategories] = useState([]);
	const [myFilters, setMyFilters] = useState({
		filters: {category: [], price: []}
	});
	const [skip, setSkip] = useState(0);
	const [limit, setLimit] = useState(6);
	const [error, setError] = useState(false);
	const [filteredResults, setfilteredResults] = useState([]);

	const init = () => {
		getCategories().then(data => {
			if (data.error) {
				setError(data.error);
			} else {
				setCategories(data);
			}
		})
	}

	const loadFilteredResults = myFilters => {
		getFilteredProducts(skip, limit, myFilters)
			.then(data => {
				if (data.error) {
					setError(data.error)
				} else {
					setfilteredResults(data.data)
				}
			})
	}

	const handleFilters = (filters, filterBy) => {
		const newFilters = {...myFilters};
		newFilters.filters[filterBy] = filters;
		loadFilteredResults(newFilters.filters);
		setMyFilters(newFilters);
	}

	useEffect(() => {
		init();
		loadFilteredResults(skip, limit, myFilters.filters)
	}, [])

	return (
		<Layout 
				title = "Shop Page"
				description="Search and find books of you" 
				className="container-fluid"
		>
			<div className="row">
				<div className="col-4">
					<h4>Filter by categories</h4>
					<ul>
						<Checkbox 
							categories={categories} 
							handleFilters={filter => handleFilters(filter, "category")} 
						/>
					</ul>
					<h4>Filter by prices</h4>
					<ul>
						<RadioBox 
							prices={prices}
							handleFilters={filter => handleFilters(filter, "price")}
						/>
					</ul>
				</div>
				<div className="col-8">
					<h2>Products</h2>
					<div className="row">
						{
							filteredResults.length === 0 ? (
								<h5>Loading...</h5>
								) : (
									filteredResults.map((p, i) => (
										<div className="col-4 mb-3" key={i} >
											<Card product={p} />
										</div>
									))
							)
						}
					</div>
				</div>
			</div>
		</Layout>
	)
}

export default Shop;